// Предположим, на клиент c сервера приходит массив объектов имеющих следующую структуру
const obj = {
    id: "id0",
    fields: {
        // поля могут содержать только строковые или числовые значения
        // некоторые поля могут иметь одинковые значения в разных обьекта
        // дополнительной вложенности нет
    }
}

// Так же имеется сам фильтр:
const filter = new Map();

// Мы можем гарантировать, что имена ключей obj.fields совпадают с именами ключей
// попадающими в filter

// Мы так же можем гарантировтаь, что значение по ключу в filter
// может содержать лишь часть значения по тому же ключу из obj.fields

// Напишите рекурсивную функцию фильтрации массива обьектов со структурой obj по значениям filter.
//-------------------------------------------------------------------------------

arrayFromServer = [
    {id: 'id0', fields: {field1: 1, field2: 2}},
    {id: 'id1', fields: {field1: 5, field2: 7}},
    {id: 'id2', fields: {field1: 5, field2: 9}}
];

const filterRecursion = (array, filter) => {
    let result = [];

    array.map( item => {
        result.push(item.fields.field1)
    })

    console.log(`result: ${result}`)

    let f = new Map([['a',1], ['b',2]])
    for (let entry of f) { 
        console.log(entry)
    }

    for ( let keys of f.keys()) {
        console.log(keys)
    }

    for ( let values of f.values()) {
        console.log(values)
    }
}

filterRecursion(arrayFromServer, filter);

// не до конца понял задание (((
