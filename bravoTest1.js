// Предположим, что в ответ на наш запрос сервер присылает массив Arr состоящий из обьектов
// const obj = {
//     id: "id0",
//     name: "",
//     info: {
//         email: "",
//         adress: "",
//     }
// }

//Напишите функцию, которая будет принимать массив Arr в качестве аргумента
//Внутри фукнциии реализуйте итерацию по этому массиву, на каждом шаге итерации
//в консоль должна выводиться строка содержащая информацию о свойствах name,
//info.email, info.adress обьекта obj, например:
//имя: obj.name, e-mail: obj.info.email, адрес:obj.info.adress

const Arr = [
     { id: "id0", name: "a", info: { email: "a@gmail.com",  adress: "www.a.com" }},
     { id: "id1", name: "b", info: { email: "b@gmail.com",  adress: "www.b.com" }},
     { id: "id2", name: "c", info: { email: "c@gmail.com",  adress: "www.c.com" }},
]

const arrHandler = (Arr) => {
    Arr.forEach( item => {
        console.log(`имя: ${item.name}, e-mail: ${item.info.email}, адрес: ${item.info.adress}`)
    })
}

arrHandler(Arr);
