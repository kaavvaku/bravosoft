import React from "react";
import { useForm } from "react-hook-form";
import { useState } from 'react';
import s from './components.module.css'

const ReactForm = ({addTableItem}) => {
    const [userInput, setUserInput] = useState('');

    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const onSubmit = (e) => {
        // e.preventDefault();
        addTableItem(userInput);
    }

    // const handleChange = (e) => {
    //     setUserInput(e.currentTarget.value)
    //     console.log('userInput', userInput)
    //     // e.preventDefault();
    // }

    const handleSubmitt = (e) => {
        e.preventDefault()
        addTableItem(userInput)
        setUserInput('')
    }

    // const handleChange = (e) => {
    //     setUserInput(e.currentTarget.value)
    // }

    return (
        <form className={s.form} onSubmit={handleSubmitt(onSubmit)}>
            <input value={userInput}
                   type='text'
                //    onChange={handleChange}
                   className={s.input}
                   placeholder={'plese enter your data ...'}
                   {...register("example")}
            />
            <input className={s.button} type="submit" value={'add'}/>
        </form>
    );
}

export default ReactForm;