import { useState } from 'react';
import s from './components.module.css'


const TableForm = ({addTableItem}) => {
    const [ userInput, setUserInput ] = useState('')

    const handleSubmit = (e) => {
        e.preventDefault()
        addTableItem(userInput)
        setUserInput('')
    }

    const handleChange = (e) => {
        setUserInput(e.currentTarget.value)
    }

    return (
        <form  className={s.form} onSubmit={handleSubmit}>
            <input className={s.input}
                placeholder={'Please, enter your data...'}
                type='text'
                value={userInput}
                onChange={handleChange}
            />
            <button className={s.button} type='submit'>Добавить</button>
        </form>
    )
}

export default TableForm;