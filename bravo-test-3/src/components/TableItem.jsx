import s from './components.module.css'

const TableItem = ({tableItem, removeTableItem, changeTableItem}) => {

    const changeHandler = (id) => {
        changeTableItem(tableItem.id)
    }
    const removeHandler = (id) => {
        removeTableItem(tableItem.id)
    }

    return (
        <div className={s.divTableItem}>
            <span className={s.in}>{tableItem.data}</span>
            <button className={s.button} onClick={changeHandler}>Изменить</button>
            <button className={s.button} onClick={removeHandler}>Удалить</button>
        </div>
    )
}

export default TableItem;