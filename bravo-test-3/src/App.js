import { useState } from 'react';
import TableItem from "./components/TableItem";
import TableForm from "./components/TableForm";
import ReactForm from './components/ReactForm'
import s from './components/components.module.css'


function App() {
    const [table, setTable] = useState([]);

    const addTableItem = (userInput) => {
        if(userInput) {
            const newItem = {
                id: Math.random().toString(30).substr(2,9),
                data: userInput,
            }
            setTable([...table, newItem] )
        }
    }

    const removeTableItem = (id) => {
        setTable(table.filter( tableItem =>  tableItem.id !== id ))
    }

    const changeTableItem = (id) => {
        return null;
    }

    return (
        <div className={s.wrapper}>
            <header className="App-header">
                <h1 className={s.h1}>Таблица из {table.length} элементов</h1>
                {/* <ReactForm addTableItem={addTableItem}/> */}
                <TableForm addTableItem={addTableItem}/>
                {table.map( tableItem => {
                    return (
                        <TableItem removeTableItem={removeTableItem}
                                   tableItem={tableItem}
                                   changeTableItem={changeTableItem}
                                   key={tableItem.id}/>
                    )
                })}
            </header>
        </div>
    );
}

export default App;
